package SimpleUserService

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

type UserService struct {
	Address string
}

type Email struct {
	Subject  string
	HtmlBody string
	TextBody string
}

func (u UserService) ValidateSession(function func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		username, err := r.Cookie("username")
		if err != nil {
			// resp, _ := json.Marshal(UserCookie{"", ""})
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		sessionToken, err := r.Cookie("session")
		if err != nil {
			// resp, _ := json.Marshal(UserCookie{"", ""})
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		forwardR, err := http.NewRequest(http.MethodGet, validateroute(u.Address), nil)
		if err != nil {
			fmt.Println(err.Error())
			http.Error(w, createErrorRespJson("Internal Server Error"), http.StatusInternalServerError)
			return
		}
		forwardR.AddCookie(&http.Cookie{Name: "username", Value: username.Value})
		forwardR.AddCookie(&http.Cookie{Name: "session", Value: sessionToken.Value})

		client := http.Client{}
		resp, err := client.Do(forwardR)
		if err != nil {
			fmt.Println(err.Error())
			http.Error(w, createErrorRespJson("Internal Server Error"), http.StatusInternalServerError)
			return
		}

		if resp.StatusCode == http.StatusOK {
			// add header as user cookie
			function(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
		}
	}
}

func (u UserService) SendEmail(email Email, username string, session string) error {
	requestBody, _ := json.Marshal(email)
	body := bytes.NewReader(requestBody)
	sendEmailRequest, err := http.NewRequest(http.MethodPost, sendemailroute(u.Address), body)
	if err != nil {
		return err
	}
	sendEmailRequest.AddCookie(&http.Cookie{Name: "username", Value: username})
	sendEmailRequest.AddCookie(&http.Cookie{Name: "session", Value: session})

	client := http.Client{}
	resp, err := client.Do(sendEmailRequest)
	if err != nil {
		return err
	}

	if resp.StatusCode == http.StatusOK {
		// add header as user cookie
		return nil
	} else {
		return errors.New(string(resp.StatusCode))
	}
}

func validateroute(address string) string {
	return fmt.Sprintf("%s/api/userservice/v0/validatesession", address)
}

func sendemailroute(address string) string {
	return fmt.Sprintf("%s/api/userservice/v0/sendemail", address)
}

func createErrorRespJson(e string) string {
	errJson, _ := json.Marshal(errorResponse{e})
	return string(errJson)
}

type errorResponse struct {
	Error string
}
